# aws-manage-test1

## How to run
Clone this repo

To create instance
```sh
./aws-manage.py create
```

To terminate instance and its associated volumes:
```sh
./aws-manage.py rm <aws instance id>
```

To list all the instances within a region:
```sh
./aws-manage.py ls
```

Functions need the `EC2REGION` environment variable to be setup
according to the content of ~/.aws/credentials.

----
For CI test of this script there is another function within script
```sh
./aws-manage.py cirun
```

----

*Refer* https://linuxacademy.com/howtoguides/posts/show/topic/14209-automating-aws-with-python-and-boto3

Example:
``` sh
AWS Access Key ID [None]: AKIAJFUD42GXIN4SQRKA
AWS Secret Access Key [None]: LLL1tjMJpRNsCq23AXVtZXLJhvYkjHeDf4UO9zzz
Default region name [None]: us-west-2
Default output format [None]: text
```
