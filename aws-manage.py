#!/usr/bin/env python

import boto3
import os
import sys

if len(sys.argv) < 2:
    print("usage: {0} <function> [arguments]")
    sys.exit(1)

region = os.environ.get('EC2REGION')
ci_instance_id = None

if region is None:
    print("Please add a region or set the EC2REGION environment variable.")
    sys.exit(1)

print("> working on region: {0}".format(region))


def init_session(r=None):
    if r is None:
        r = region
    s = boto3.Session(region_name=region)
    return s.resource('ec2')


def ls():
    ec2 = init_session()
    for i in ec2.instances.all():
        print("{0} / {1} ({2}) - [{3}]".format(
            region, i.id, i.instance_type, i.state['Name']
        ))


def create():
    print("creating one EC2 instance in region: {0}".format(region))
    try:
        ec2 = init_session()
        instance = ec2.create_instances(
            ImageId='ami-00e17d1165b9dd3ec',
            MinCount=1,
            MaxCount=1,
            InstanceType='t2.micro'
        )
        instance[0].wait_until_running()
        print('instance created: {0}'.format(instance[0].id))
        return instance[0].id
    except Exception as error:
        print('error creating EC2 instance')
        print(error)
        sys.exit(1)


def rm(ins_id=None):
    if ins_id is None:
        if len(sys.argv) < 3:
            print("usage: {0} rm <aws instance id>".format(sys.argv[1]))
            sys.exit(1)
        else:
            del_id = sys.argv[2]
    else:
        del_id = ins_id

    ec2 = init_session()

    try:
        ec2.instances.filter(InstanceIds=[del_id]).terminate()
        print('Sent request to delete instance ID {0}'.format(del_id))
    except Exception as error:
        print('error while terminating {0}'.format(del_id))
        print(error)
        sys.exit(1)


def cirun():
    ins_id = create()
    ls()
    rm(ins_id)

    
if __name__ == '__main__':
    getattr(sys.modules[__name__], sys.argv[1])()
